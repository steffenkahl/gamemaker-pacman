if (obj_GameManager.countdownIsRunning = false) //IF COUNTDOWN HAS ENDED
{
	//GET DIRECTION
	if(keyboard_check(ord("W")) || keyboard_check_pressed(vk_up)){
		moveY = -1;
		moveX = 0;
		image_angle = 90;
	}
	else if(keyboard_check(ord("S")) || keyboard_check_pressed(vk_down)){
		moveY = 1;
		moveX = 0;
		image_angle = 270;
	}
	else if(keyboard_check(ord("A")) || keyboard_check_pressed(vk_left)){
		moveX = -1;
		moveY = 0;
		image_angle = 180;
	}
	else if(keyboard_check(ord("D")) || keyboard_check_pressed(vk_right)){
		moveX = 1;
		moveY = 0;
		image_angle = 0;
	}
}

if(x < objectPosX) {
	x += 1;
}
if(y < objectPosY) {
	y += 1;
}
if(x > objectPosX) {
	x -= 1;
}
if(y > objectPosY) {
	y -= 1;
}

move_wrap(true,true,sprite_width/2);

//Wrapping doesnt work yet!