//=== Determine Width and Height ==//
var _spr = spr_level,
	_ww = sprite_get_width(_spr),
	_hh = sprite_get_height(_spr);

//=== Create Surface ===//
var _surf=surface_create(_ww, _hh);
surface_set_target(_surf);
draw_clear_alpha(0, 0);		
draw_sprite(_spr, -1, 0, 0);
surface_reset_target();

//=== Surface to Buffer ===//
_buff=buffer_create(4 * _ww * _hh, buffer_fixed, 1);
buffer_get_surface(_buff, _surf, 0, 0, 0);
surface_free(_surf);// we don't need the surface anymore

//=== Extracting Color Info ===//
// this is an example for a single pixel, you can of course loop all pixels, or access only specific ones!
// Pixel coordinates [x,y] start at [0,0] and should not exceed [_ww-1,_hh-1]

for(_X_= 0; _X_ < _ww; _X_++) 
{
	for(_Y_= 0; _Y_ < _hh; _Y_++) 
	{
		var _x = _X_,
		_y = _Y_,
		pixel = buffer_peek(_buff, 4 * (_x + _y * _ww), buffer_u32),	// extracts info in ABGR Format
		a = (pixel >> 24) & $ff,	// Alpha [0-255]	
		r = pixel & $ff,			// Red [0-255]	
		g = (pixel >> 8) & $ff,		// Green [0-255]	
		b = (pixel >> 16) & $ff;	// Blue [0-255]	
				
		if(a >= 255 && r == 0 && g == 0 && b == 0){
			obj_map.newCollisionBox(_X_,_Y_);
		}
		
		
	}	
}

// === Cleanup ===//
buffer_delete(_buff);