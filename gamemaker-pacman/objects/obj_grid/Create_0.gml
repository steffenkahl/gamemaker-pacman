// Create Grid
var cell_width = 30;
var cell_height = 30;

var hcells = room_width div cell_width;
var vcells = 19;

global.grid = mp_grid_create(0, 0, hcells, vcells, cell_width, cell_height);

//Add Walls
mp_grid_add_instances(global.grid, obj_wall, false);