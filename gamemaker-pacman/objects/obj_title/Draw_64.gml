draw_set_alpha(1);
draw_set_halign(fa_center);
draw_set_valign(fa_center);

draw_set_font(fnt_menuoption);

for (i = 0; i< array_length_1d(menu_options); i++) 
{
	if(selectedOption = i) {
		draw_set_alpha(1);
	} else {
		draw_set_alpha(0.5);
	}
	draw_text(room_width / 2, (room_height / 2) + 40*i,menu_options[i]);
}

draw_set_alpha(1);