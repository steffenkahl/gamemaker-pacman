//SELECT MENU OPTION
if (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up)){
	selectedOption -=1;	
}

if (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down)){
	selectedOption +=1;	
}

//ROLL OVER IF MENUOPTIONS ARE EXCEEDED
if(selectedOption > array_length_1d(menu_options) -1) {
	selectedOption = 0;	
}

if(selectedOption < 0){
	selectedOption = array_length_1d(menu_options) -1;
}

//SELECT AN OPTION
if(keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter)) {
	switch(menu_options[selectedOption]) {
	
		case "START GAME":
			room_goto(rm_game);
		break;
		
		case "Credits":
			room_goto(rm_credits);
		break;
		
		case "End Game":
			game_end();
		break;
	}
}