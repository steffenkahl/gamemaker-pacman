draw_set_alpha(1);
draw_set_halign(fa_center);
draw_set_valign(fa_center);

draw_set_font(fnt_menuoption);

startX = 350;

for (i = 0; i< array_length_1d(menu_options); i++) 
{
	draw_set_alpha(1);
	draw_text(room_width / 2, startX + 30*i,menu_options[i]);
}

draw_set_alpha(1);