//CREATE NEW PATH
path = path_add();

//SET IF DEATH
isDead = false;

randomize();

targetPoint[0] = obj_player.grid_x;
targetPoint[1] = obj_player.grid_y;
	
function FindNewTarget()
{
	if(!isDead)
	{
		targetPoint[0] = obj_player.grid_x;
		targetPoint[1] = obj_player.grid_y;
	}
	else
	{
		targetPoint[0] = 9;
		targetPoint[1] = 9;
	}
}

alarm_interval = 30;
alarm[0] = alarm_interval;