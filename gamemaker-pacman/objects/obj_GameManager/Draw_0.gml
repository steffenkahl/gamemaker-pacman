draw_set_color(c_gray);
draw_set_halign(fa_center);
draw_text(room_width / 2, room_height/2 + 10, timeText);

draw_set_color(c_white);
draw_text(room_width / 2, room_height - 60, "SCORE: " + string(playerPoints));

draw_set_color(c_white);
draw_text(room_width / 2, room_height - 30, "LIFES: " + string(lives));