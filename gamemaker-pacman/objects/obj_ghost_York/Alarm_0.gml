show_debug_message("Test 1");

mx = (targetPoint[0] - 1) * 30 + 15;
my = (targetPoint[1] - 1) * 30 + 15;

if(point_distance(x,y,mx,my) < 30)
{
	FindNewTarget();
}

if (mp_grid_path(global.grid, path, x,y, mx, my,0)) {
	show_debug_message("Test 2");
	path_start(path, 1.5, path_action_stop, false);
} else {
	FindNewTarget();
}

alarm[0] = alarm_interval;