//CREATE NEW PATH
path = path_add();

//SET IF DEATH
isDead = false;

randomize();

targetPoint[0] = 0;
targetPoint[1] = 0;
	
function FindNewTarget()
{
	if (!isDead) {
		targetPoint[0] = 0;
		targetPoint[1] = 0;
	
		targetPointIsSolid = true;
	
		while(targetPointIsSolid = true){ //Zielpunkte ausprobieren bis es keine Wand ist
		
			targetPointIsSolid = false; //Standardmäßig wird der Zielpunkt keine Wand sein
		
			targetPoint[0] = irandom_range(1,17);
			targetPoint[1] = irandom_range(1,19);
		
			if(scr_testCollision(targetPoint[0], targetPoint[1]))
			{
				targetPointIsSolid = true; //Setzen wenn der Zielpunkt eine Wand ist
				break;
			}
		
			if (targetPoint[0] == (x div 30) * 30 && targetPoint[1] == (y div 30) * 30) {
				targetPointIsSolid = true; //Setzen wenn der Zielpunkt die aktuelle Position ist
			}
		}
	}
	else
	{
		targetPoint[0] = 9;
		targetPoint[1] = 9;
	}
}

alarm_interval = 10;
alarm[0] = alarm_interval;