function scr_pathfinder(curPosX, curPosY)
{
	show_debug_message("1 - Setting Variables");
	currentPosition[0] = curPosX;
	currentPosition[1] = curPosY;
	
	targetPointIsSolid = true;
	
	show_debug_message("2 - Testing if targetPoint is ok");
	while(targetPointIsSolid = true){ //Zielpunkte ausprobieren bis es keine Wand ist
		
		targetPointIsSolid = false; //Standardmäßig wird der Zielpunkt keine Wand sein
		
		targetPoint[0] = irandom_range(1,17);
		targetPoint[1] = irandom_range(1,19);
		
		if(scr_testCollision(targetPoint[0], targetPoint[1]))
		{
			targetPointIsSolid = true; //Setzen wenn der Zielpunkt eine Wand ist
			break;
		}
		
		if (targetPoint[0] == currentPosition[0] && targetPoint[1] == currentPosition[1]) {
			targetPointIsSolid = true; //Setzen wenn der Zielpunkt die aktuelle Position ist
		}
	}
	show_debug_message("2 - Result: " + string(targetPoint[0]) + " | " + string(targetPoint[1]));
	show_debug_message("3 - Start getting Path");
	//START GETTING PATH
	pathComplete = false;
	
	potentialPoint[0] = currentPosition[0]; //Setze X des potentialPoint auf X vom Geist
	potentialPoint[1] = currentPosition[1];	//Setze X des potentialPoint auf X vom Geist
	
	pathPointList[0,0] = 0; //X-Position
	pathPointList[0,1] = 0; //Y-Position
	
	randomize(); //Sicherstellen, dass die Route bei jedem Starten des Spiels anders ist
	
	
	pathPointListRESET = false;
	show_debug_message("4.1 - Setting Variables |||||||||||||||||| FIRST TRY");
	potentialPoint[0] = currentPosition[0];
	potentialPoint[1] = currentPosition[1];
	pathPointList[0,0] = 0;
			
	show_debug_message("4 - Calculating Path");
	//Pfad berechnen
	lastDirection = 4;
	while (!pathComplete) 
	{
		show_debug_message("4.1 - Path is not Complete");
		if(pathPointListRESET)
		{
			pathPointList = 0;
			show_debug_message("4.1 - Setting Variables |||||||||||||||||| NEW TRY");
			potentialPoint[0] = currentPosition[0];
			potentialPoint[1] = currentPosition[1];
			pathPointList[0,0] = 0;
			pathPointListRESET = false;
		}
		
		
		show_debug_message("4.2 - Set Direction");
		isDirectionOK = false;
		while(!isDirectionOK)
		{
			show_debug_message("4.2.1 - Test Directions");
			directionIDok = false;
			
			while (directionIDok == false) {
				show_debug_message("Test");
				randomize();
				randomDirection = irandom_range(0,3); //Zufällige Richtung
				
				switch (lastDirection) {
					//CHECKEN, dass die neue Richtung nicht entgegengesetzt zur letzten steht
					case 0: 
						if(randomDirection != 1) {
							directionIDok = true;
						}
					break;
					
					case 1: 
						if(randomDirection != 0) {
							directionIDok = true;
						}
					break;
					
					case 2: 
						if(randomDirection != 3) {
							directionIDok = true;
						}
					break;
					
					case 3: 
						if(randomDirection != 2) {
							directionIDok = true;
						}
					break;
					
					case 4:
						directionIDok = true;
					break;
				}
				
			}
			puffer = 2;
		
			switch (randomDirection) //Je nach Wert soll eine Richtung ausprobiert werden
			{
				case 0:
					//Richtung: Rechts
					if(scr_testCollision(currentPosition[0] + 1, currentPosition[1]))
					{
						isDirectionOK = false;
					} else {
						if(point_distance(targetPoint[0], targetPoint[1], potentialPoint[0] + 1, potentialPoint[1]) < point_distance(targetPoint[0], targetPoint[1], potentialPoint[0] + 1, potentialPoint[1]) + puffer) {
							potentialPoint[0] += 1;
							isDirectionOK = true;
						} else {
							isDirectionOK = false;
						}
					}
				break;
			
				case 1:
					//Richtung: Links
					if(scr_testCollision(currentPosition[0] - 1, currentPosition[1]))
					{
						isDirectionOK = false;
					} else {
						if(point_distance(targetPoint[0], targetPoint[1], potentialPoint[0] - 1, potentialPoint[1]) < point_distance(targetPoint[0], targetPoint[1], potentialPoint[0], potentialPoint[1]) + puffer) {
							potentialPoint[0] -= 1;
							isDirectionOK = true;
						} else {
							isDirectionOK = false;
						}
					}
				break;
					
				case 2:
					//Richtung: Runter
					if(scr_testCollision(currentPosition[0], currentPosition[1] + 1))
					{
						isDirectionOK = false;
					} else {
						if(point_distance(targetPoint[0], targetPoint[1], potentialPoint[0], potentialPoint[1] + 1) < point_distance(targetPoint[0], targetPoint[1], potentialPoint[0], potentialPoint[1]) + puffer){
							potentialPoint[1] += 1;
							isDirectionOK = true;
						} else {
							isDirectionOK = false;
						}
					}
				break;
			
				case 3:
					//Richtung: Hoch
					if(scr_testCollision(currentPosition[0], currentPosition[1] - 1))
					{
						isDirectionOK = false;
					} else {
						if(point_distance(targetPoint[0], targetPoint[1], potentialPoint[0] + 1, potentialPoint[1] - 1) < point_distance(targetPoint[0], targetPoint[1], potentialPoint[0], potentialPoint[1]) + puffer){
							potentialPoint[1] += 1;
							isDirectionOK = true;
						} else {
							isDirectionOK = false;
						}
					}
				break;
			}
		}
		
		
		
		show_debug_message("4.3 - Testing if in List or Target | potentialPoint: " + string(potentialPoint[0]) + "-" + string(potentialPoint[1]));
		isAlreadyInList = false;
		
		if(!pathPointList[0,0] == 0) 
		{ //wenn der nullte Wert der Liste gesetzt ist, soll geguckt werdn, ob der Punkt schon in der Liste ist
			for(i = 0; i < array_length(pathPointList); i++)
			{
				if(potentialPoint[0] == pathPointList[i,0])
				{
					if(potentialPoint[1] == pathPointList[i,1])
					{	
						show_debug_message("4.3 - Testing Values: " + string(potentialPoint[0]) + "-" + string(potentialPoint[1]) + " | " + string(pathPointList[i,0]) + "-" + string(pathPointList[i,1]));
						pathPointListRESET = true;
						isAlreadyInList = true;
					}
				}
			}
		}
		
		if(array_length(pathPointList) > 5) {
			show_debug_message("Path is over 5 Steps long");
		}
		
		show_debug_message("4.4 - NEED TO WRITE IN LIST");
		if(!isAlreadyInList)
		{
			if(pathPointList[0,0] == 0) //wenn der nullte Wert der Liste nicht gesetzt ist, soll der nullte Punkt gesetzt werden
			{
				pathPointList[0,0] = potentialPoint[0];
				pathPointList[0,1] = potentialPoint[1];
				show_debug_message("4.4 - Writing Point |||||||||||||||| SUCCESS!");
			} 
			else if(pathPointList[0,0] != 0) //wenn der nullte Wert der Liste gesetzt ist, soll der nächste Punkt gesetzt werden
			{
				pathPointList[array_length(pathPointList),0] = potentialPoint[0]; //Setze X
				pathPointList[array_length(pathPointList) -1,1] = potentialPoint[1]; //Setze Y
			}
		}
		
		show_debug_message("4.5 - Testing if is Target Point");
		//wenn der getestete Punkt der targetPoint ist, ist der Weg komplett
		if(potentialPoint[0] == targetPoint[0] && potentialPoint[1] == targetPoint[1]) 
		{
			show_debug_message("4.5.1 - PATH COMPLETE!!!! WOOHOO!");
			pathPointList[array_length(pathPointList),0] = "ENDE";
			pathComplete = true;	
		}
	}
	show_debug_message("4 - Path calculated");
	
	return pathPointList;
}