function scr_testCollision(testPositionX, testPositionY)
{
	isCollision = false;
	
	for (i = 0; i < array_length(obj_map.collisionBoxX); i++)
	{
		if (testPositionX == obj_map.collisionBoxX[i] && testPositionY == obj_map.collisionBoxY[i])
		{
			isCollision = true;
		}
	}
	
	return isCollision; //gibt true zurück, wenn es eine Kollision gibt
}